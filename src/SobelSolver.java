import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SobelSolver {

    private static final int PIXEL_POSITION_START = 1;
    private static final int INDEX_OFFSET = 1;

    public static void main(String[] args) {
        SobelSolver sobelSolver = new SobelSolver();

        System.out.println("Question Three:");
        List<List<Integer>> imageFragmentQuestionThree = sobelSolver.generateImageFragmentQuestionThree();
        performSobelOperator(imageFragmentQuestionThree);

        System.out.println();

        System.out.println("Question Seven:");
        List<List<Integer>> imageFragmentQuestionSeven = sobelSolver.generateImageFragmentQuestionSeven();
        performSobelOperator(imageFragmentQuestionSeven);
    }

    private List<List<Integer>> generateImageFragmentQuestionThree() {
        List<List<Integer>> imageFragment = new ArrayList<>();
        List<Integer> rowOne = Arrays.asList(10, 15, 25, 35);
        List<Integer> rowTwo = Arrays.asList(15, 20, 40, 75);
        List<Integer> rowThree = Arrays.asList(25, 40, 90, 120);
        List<Integer> rowFour = Arrays.asList(20, 70, 120, 180);
        imageFragment.addAll(Arrays.asList(rowOne, rowTwo, rowThree, rowFour));
        return imageFragment;
    }

    private List<List<Integer>> generateImageFragmentQuestionSeven() {
        List<List<Integer>> imageFragment = new ArrayList<>();
        List<Integer> rowOne = Arrays.asList(0, 1, 2, 0, 4, 5);
        List<Integer> rowTwo = Arrays.asList(0, 0, 1, 1, 4, 5);
        List<Integer> rowThree = Arrays.asList(0, 2, 0, 4, 5, 4);
        List<Integer> rowFour = Arrays.asList(0, 0, 5, 4, 6, 6);
        List<Integer> rowFive = Arrays.asList(0, 0, 6, 6, 5, 6);
        List<Integer> rowSix = Arrays.asList(5, 4, 6, 5, 4, 5);
        imageFragment.addAll(Arrays.asList(rowOne, rowTwo, rowThree, rowFour, rowFive, rowSix));
        return imageFragment;
    }

    private static void performSobelOperator(List<List<Integer>> imageFragment) {
        int pixelPositionEnd = imageFragment.size() - 1;
        for (int rowIndex = PIXEL_POSITION_START; rowIndex < pixelPositionEnd; rowIndex++) {
            for (int columnIndex = PIXEL_POSITION_START; columnIndex < pixelPositionEnd; columnIndex++) {
                SobelMask sobelMask = new SobelMask(imageFragment, rowIndex, columnIndex);
                System.out.print("Pixel Location (" + (rowIndex + INDEX_OFFSET) + ", " + (columnIndex + INDEX_OFFSET) + "), ");
                System.out.print("Grey Scale Intensity: " + sobelMask.greyScaleIntensity() + " ---------> ");

                double gradient = sobelMask.calculateGradient();
                System.out.print("Gradient Magnitude: " + gradient + " ---------> ");

                double orientation = sobelMask.calculateOrientation();
                System.out.println("Gradient Orientation: " + orientation);
            }
        }
    }

}
