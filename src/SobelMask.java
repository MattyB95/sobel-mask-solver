import java.util.List;

class SobelMask {

    private int z1;
    private int z2;
    private int z3;
    private int z4;
    private int z5;
    private int z6;
    private int z7;
    private int z8;
    private int z9;

    SobelMask(List<List<Integer>> imageFragment, int rowIndex, int columnIndex) {
        z1 = imageFragment.get(rowIndex - 1).get(columnIndex - 1);
        z2 = imageFragment.get(rowIndex - 1).get(columnIndex);
        z3 = imageFragment.get(rowIndex - 1).get(columnIndex + 1);
        z4 = imageFragment.get(rowIndex).get(columnIndex - 1);
        z5 = imageFragment.get(rowIndex).get(columnIndex);
        z6 = imageFragment.get(rowIndex).get(columnIndex + 1);
        z7 = imageFragment.get(rowIndex + 1).get(columnIndex - 1);
        z8 = imageFragment.get(rowIndex + 1).get(columnIndex);
        z9 = imageFragment.get(rowIndex + 1).get(columnIndex + 1);
    }

    int greyScaleIntensity() {
        return z5;
    }

    double calculateGradient() {
        int gradientInX = (z7 + (2 * z8) + z9) - (z1 + (2 * z2) + z3);
        int gradientInY = (z3 + (2 * z6) + z9) - (z1 + (2 * z4) + z7);
        return Math.sqrt(Math.pow(gradientInX, 2) + Math.pow(gradientInY, 2));
    }

    double calculateOrientation() {
        int opposite = (z3 + (2 * z6) + z9) - (z1 + (2 * z4) + z7);
        int adjacent = (z7 + (2 * z8) + z9) - (z1 + (2 * z2) + z3);
        double atan2 = Math.atan2(opposite, adjacent);
        return Math.toDegrees(atan2);
    }

}
