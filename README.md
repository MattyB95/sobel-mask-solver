# Sobel Mask Solver

A quick program I made because I did not want to have to solve the below questions by hand. This way seemed more fun and interesting.

![alt text](res/Question3.jpg "Question")

![alt text](res/Question7.jpg "Question")

# Program Output and (Hopefully!) Answer to Question

Question Three:

Pixel Location (2, 2), Grey Scale Intensity: 20 ---------> Gradient Magnitude: 183.84776310850236 ---------> Gradient Orientation: 45.0

Pixel Location (2, 3), Grey Scale Intensity: 40 ---------> Gradient Magnitude: 318.90437438203946 ---------> Gradient Orientation: 41.18592516570965

Pixel Location (3, 2), Grey Scale Intensity: 40 ---------> Gradient Magnitude: 315.03968004046726 ---------> Gradient Orientation: 54.03948280335512

Pixel Location (3, 3), Grey Scale Intensity: 90 ---------> Gradient Magnitude: 452.6035793053343 ---------> Gradient Orientation: 45.895173710211076


Question Seven:

Pixel Location (2, 2), Grey Scale Intensity: 0 ---------> Gradient Magnitude: 4.0 ---------> Gradient Orientation: 90.0

Pixel Location (2, 3), Grey Scale Intensity: 1 ---------> Gradient Magnitude: 3.1622776601683795 ---------> Gradient Orientation: 71.56505117707799

Pixel Location (2, 4), Grey Scale Intensity: 1 ---------> Gradient Magnitude: 14.7648230602334 ---------> Gradient Orientation: 61.69924423399363

Pixel Location (2, 5), Grey Scale Intensity: 4 ---------> Gradient Magnitude: 13.92838827718412 ---------> Gradient Orientation: 68.96248897457819

Pixel Location (3, 2), Grey Scale Intensity: 2 ---------> Gradient Magnitude: 7.211102550927978 ---------> Gradient Orientation: 56.309932474020215

Pixel Location (3, 3), Grey Scale Intensity: 0 ---------> Gradient Magnitude: 14.212670403551895 ---------> Gradient Orientation: 39.28940686250036

Pixel Location (3, 4), Grey Scale Intensity: 4 ---------> Gradient Magnitude: 18.439088914585774 ---------> Gradient Orientation: 49.398705354995535

Pixel Location (3, 5), Grey Scale Intensity: 5 ---------> Gradient Magnitude: 10.0 ---------> Gradient Orientation: 36.86989764584402

Pixel Location (4, 2), Grey Scale Intensity: 0 ---------> Gradient Magnitude: 16.1245154965971 ---------> Gradient Orientation: 82.8749836510982

Pixel Location (4, 3), Grey Scale Intensity: 5 ---------> Gradient Magnitude: 20.0 ---------> Gradient Orientation: 53.13010235415598

Pixel Location (4, 4), Grey Scale Intensity: 4 ---------> Gradient Magnitude: 11.661903789690601 ---------> Gradient Orientation: 30.96375653207352

Pixel Location (4, 5), Grey Scale Intensity: 6 ---------> Gradient Magnitude: 5.656854249492381 ---------> Gradient Orientation: 45.0

Pixel Location (5, 2), Grey Scale Intensity: 0 ---------> Gradient Magnitude: 22.80350850198276 ---------> Gradient Orientation: 52.1250163489018

Pixel Location (5, 3), Grey Scale Intensity: 6 ---------> Gradient Magnitude: 18.384776310850235 ---------> Gradient Orientation: 67.61986494804043

Pixel Location (5, 4), Grey Scale Intensity: 6 ---------> Gradient Magnitude: 3.1622776601683795 ---------> Gradient Orientation: -71.56505117707799

Pixel Location (5, 5), Grey Scale Intensity: 5 ---------> Gradient Magnitude: 4.47213595499958 ---------> Gradient Orientation: 153.434948822922
